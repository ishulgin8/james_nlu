ajax_bean =
  start_ajax_call: (url, method, send_data, data_type, headers) ->
    send_data_type = 'json'
    send_method = "POST"
    send_method = method  if typeof (method) isnt "undefined"
    send_data_type = data_type  if data_type
    send_data = []  if typeof (send_data) is "undefined"
    all_back_data =
      error_back_data: null
      complete_back_data: null
      success_back_data: null

    $.ajax
      headers: headers
      url: url
      type: send_method
      async: false
      data: send_data
      dataType: send_data_type
      error: (jqxhr, text_status, error_status) ->
        all_back_data.error_back_data = jqxhr
        return

      complete: (jqxhr, text_status) ->
        all_back_data.complete_back_data = jqxhr
        return

      success: (data) ->
        all_back_data.success_back_data = data
        return

    all_back_data
