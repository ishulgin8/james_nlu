var config, decisionActivator, submitNluQuery;

config = {
  api_base_url: "http://digitalleap.co.uk:20888"
};

submitNluQuery = function(e) {
  var text_query;
  $(e).addClass("disabled");
  $(e).prop("disabled", true);
  $(".loader_wr").addClass("active");
  text_query = $.trim($("#ai_txt_query").val());
  setTimeout((function() {
    var ai_action, ai_team, ajax_call_res, headers, send_data;
    send_data = {
      "text_query": text_query
    };
    headers = null;
    ajax_call_res = ajax_bean.start_ajax_call(config.api_base_url + "/nlu/apiai_query", "POST", send_data, "json", headers);
    if (ajax_call_res.success_back_data && ajax_call_res.complete_back_data && ajax_call_res.complete_back_data.status === 201) {
      toastr.success("Successfully completed!");
      ai_action = ajax_call_res.success_back_data.data.result.action;
      ai_team = ajax_call_res.success_back_data.data.result.parameters.teams;
      decisionActivator(ai_action, ai_team);
    } else {
      toastr.error("" + ajax_call_res.success_back_data.error_msg);
    }
    $("#nlu_terminal_output").html(JSON.stringify(ajax_call_res.success_back_data.data.result));
    $(e).removeClass('disabled');
    $(e).prop("disabled", false);
    return $(".loader_wr").removeClass('active');
  }), 1000);
  return false;
};

decisionActivator = function(ai_action, ai_team) {
  if (ai_action) {
    toastr.success("Activate decisions event loop");
    return setTimeout((function() {
      var ajax_call_res, cursor_index, fixtures, headers, i, j, latest_fixture, len, score_line_teams, send_data, team, team_cursor, teams;
      send_data = null;
      headers = {
        "X-Auth-Token": "fe68ba8f95c440ca9502890eca00841c",
        "X-Response-Control": "minified"
      };
      ajax_call_res = ajax_bean.start_ajax_call("http://api.football-data.org/v1/soccerseasons/398/teams", "GET", send_data, "json", headers);
      if (ajax_call_res.complete_back_data && ajax_call_res.complete_back_data.status !== 500) {
        toastr.success("Successfully completed!");
        if (ajax_call_res.success_back_data) {
          teams = ajax_call_res.success_back_data.teams;
          cursor_index = -1;
          for (i = j = 0, len = teams.length; j < len; i = ++j) {
            team = teams[i];
            if (team.name === ai_team) {
              cursor_index = i;
            }
          }
          if (cursor_index !== -1) {
            team_cursor = teams[cursor_index];
            ajax_call_res = ajax_bean.start_ajax_call("http://api.football-data.org/v1/teams/" + team_cursor.id + "/fixtures", "GET", send_data, "json", headers);
            if (ajax_call_res.complete_back_data && ajax_call_res.complete_back_data.status !== 500) {
              fixtures = ajax_call_res.success_back_data.fixtures;
              latest_fixture = fixtures[fixtures.length - 1];
              score_line_teams = "<p>" + latest_fixture.awayTeamName + " - " + latest_fixture.homeTeamName + ": " + latest_fixture.result.goalsAwayTeam + " - " + latest_fixture.result.goalsHomeTeam + "</p>";
              return $("#score_line_teams").html(score_line_teams);
            } else {
              return toastr.warning("Team score is not founded");
            }
          } else {
            return toastr.warning("Team is not founded");
          }
        } else {
          return toastr.error("No footbal content");
        }
      } else {
        return toastr.error("" + ajax_call_res.success_back_data.error_msg);
      }
    }), 1000);
  } else {
    return toastr.warning("Message is not recognized");
  }
};

//# sourceMappingURL=theme.js.map
