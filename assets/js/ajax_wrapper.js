var ajax_bean;

ajax_bean = {
  start_ajax_call: function(url, method, send_data, data_type, headers) {
    var all_back_data, send_data_type, send_method;
    send_data_type = 'json';
    send_method = "POST";
    if (typeof method !== "undefined") {
      send_method = method;
    }
    if (data_type) {
      send_data_type = data_type;
    }
    if (typeof send_data === "undefined") {
      send_data = [];
    }
    all_back_data = {
      error_back_data: null,
      complete_back_data: null,
      success_back_data: null
    };
    $.ajax({
      headers: headers,
      url: url,
      type: send_method,
      async: false,
      data: send_data,
      dataType: send_data_type,
      error: function(jqxhr, text_status, error_status) {
        all_back_data.error_back_data = jqxhr;
      },
      complete: function(jqxhr, text_status) {
        all_back_data.complete_back_data = jqxhr;
      },
      success: function(data) {
        all_back_data.success_back_data = data;
      }
    });
    return all_back_data;
  }
};

//# sourceMappingURL=ajax_wrapper.js.map
