var request = require('request');
// var api_token = "8ba49646b91c45678a488b343c7f2615"; // === client api token
var api_token = "337179e773454fe9b7321a32689d05f6"; // === developer api token

module.exports = {
  apiai_get_request: function(url, qs, cb) {
    request({
      url: url,
      qs: qs,
      method: 'GET',
      headers: {
        'Authorization': 'Bearer ' + api_token
      }
    }, function(error, response, body) {
      var res_data = {
        'status': false,
        'error_msg': null,
        'data': null
      };
      if(error) {
        res_data.error_msg = error;
      } else {
        var response_json_body = JSON.parse(response.body);
        if(response.statusCode === 200) {
          res_data.status = true;
          res_data.data = response_json_body;
        } else {
          res_data.error_msg = response_json_body.status.errorDetails;
        }
      }
      cb(error, res_data);
    });
  },
  apiai_post_request: function(data, cb) {
    var url = data.url;
    var post_data = data.post_data;
    request({
      url: url,
      method: 'POST',
      headers: {
        'Authorization': 'Bearer ' + api_token,
        'Content-Type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify(post_data)
    }, function(error, response, body) {
      var res_data = {
        'status': false,
        'error_msg': null,
        'data': null
      };
      if(error) {
        res_data.error_msg = error;
      } else {
        var response_json_body = JSON.parse(response.body);
        if(response.statusCode === 200) {
          res_data.status = true;
          res_data.data = response_json_body;
        } else {
          res_data.error_msg = response_json_body.status.errorDetails;
        }
      }
      cb(error, res_data);
    });
  }
};
