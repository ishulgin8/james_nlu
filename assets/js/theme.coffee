config =
  # api_base_url: "http://127.0.0.1:20888"
  api_base_url: "http://digitalleap.co.uk:20888"

submitNluQuery = (e) ->
  $(e).addClass "disabled"
  $(e).prop "disabled", true
  $(".loader_wr").addClass "active"
  text_query = $.trim $("#ai_txt_query").val()
  setTimeout (->
    send_data =
      "text_query": text_query
    headers = null
    ajax_call_res = ajax_bean.start_ajax_call "#{config.api_base_url}/nlu/apiai_query", "POST", send_data, "json", headers
    if ajax_call_res.success_back_data and ajax_call_res.complete_back_data and ajax_call_res.complete_back_data.status is 201
      toastr.success "Successfully completed!"
      ai_action = ajax_call_res.success_back_data.data.result.action
      ai_team = ajax_call_res.success_back_data.data.result.parameters.teams
      decisionActivator ai_action, ai_team
    else
      toastr.error "#{ajax_call_res.success_back_data.error_msg}"
    $("#nlu_terminal_output").html JSON.stringify(ajax_call_res.success_back_data.data.result)
    $(e).removeClass 'disabled'
    $(e).prop "disabled", false
    $(".loader_wr").removeClass 'active'
  ), 1000
  false

decisionActivator = (ai_action, ai_team) ->
  if ai_action
    toastr.success "Activate decisions event loop"
    setTimeout (->
      send_data = null
      headers =
        "X-Auth-Token": "fe68ba8f95c440ca9502890eca00841c"
        "X-Response-Control": "minified"
      ajax_call_res = ajax_bean.start_ajax_call "http://api.football-data.org/v1/soccerseasons/398/teams", "GET", send_data, "json", headers
      if ajax_call_res.complete_back_data and ajax_call_res.complete_back_data.status isnt 500
        toastr.success "Successfully completed!"
        if ajax_call_res.success_back_data
          teams = ajax_call_res.success_back_data.teams
          cursor_index = -1
          for team, i in teams
            cursor_index = i if team.name is ai_team
          if cursor_index isnt -1
            team_cursor = teams[cursor_index]
            ajax_call_res = ajax_bean.start_ajax_call "http://api.football-data.org/v1/teams/#{team_cursor.id}/fixtures", "GET", send_data, "json", headers
            if ajax_call_res.complete_back_data and ajax_call_res.complete_back_data.status isnt 500
              fixtures = ajax_call_res.success_back_data.fixtures
              latest_fixture = fixtures[fixtures.length - 1]
              score_line_teams = "<p>#{latest_fixture.awayTeamName} - #{latest_fixture.homeTeamName}: #{latest_fixture.result.goalsAwayTeam} - #{latest_fixture.result.goalsHomeTeam}</p>"
              $("#score_line_teams").html score_line_teams
            else
              toastr.warning "Team score is not founded"
          else
            toastr.warning "Team is not founded"
        else
          toastr.error "No footbal content"
      else
        toastr.error "#{ajax_call_res.success_back_data.error_msg}"
    ), 1000
  else
    toastr.warning "Message is not recognized"
