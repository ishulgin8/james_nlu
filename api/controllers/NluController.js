/**
 * NluController
 *
 * @description :: Server-side logic for managing nlus
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var apiai_app = require('apiai')("8ba49646b91c45678a488b343c7f2615");
var shortid = require('shortid');
var async_module = require('async');
var apiai_requester = require("../services/apiai_requester");

module.exports = {
	apiai_webhook: function(req, res) {
		var d = {
			'status': true
		};
		res.contentType = 'json';
		res.status(201).send(d);
	},
	apiai_query: function(req, res) {
		var text_query = req.body.text_query;
		var url = "https://api.api.ai/v1/query?v=20150910";
		var qs = {
			'query': text_query,
			'lang': 'en'
		};
		apiai_requester.apiai_get_request(url, qs, function requesterCallback(error, d) {
	    res.contentType = 'json';
	    res.status(201).send(d);
	  });
	},
	apiai_get_entities: function(req, res) {
		var url = "https://api.api.ai/v1/entities?v=20150910";
		var qs = {};
		apiai_requester.apiai_get_request(url, qs, function requesterCallback(error, d) {
	    res.contentType = 'json';
	    res.status(201).send(d);
	  });
	},
	apiai_post_entities: function(req, res) {
		var sessionId = shortid.generate();
		var post_data = {
			"name": "Appliances",
			"sessionId": sessionId,
	    "entries": [
				{
	        "value": "Coffee Maker",
	        "synonyms": ["coffee maker", "coffee machine",  "coffee"]
		    },
				{
	        "value": "Thermostat",
	        "synonyms": ["Thermostat", "heat", "air conditioning"]
		    },
				{
	        "value": "Lights",
	        "synonyms": ["lights", "light", "lamps"]
		    },
				{
	        "value": "Garage door",
	        "synonyms": ["garage door", "garage"]
		    },
				{
					'value': 'Firefox',
          'synonyms': ['Firefox']
				}
			]
		};
		var send_data = {
			'url': "https://api.api.ai/v1/entities?v=20150910",
			'post_data': post_data
		};
		apiai_requester.apiai_post_request(send_data, function requesterCallback(error, d) {
	    res.contentType = 'json';
	    res.status(201).send(d);
	  });
	}
};
